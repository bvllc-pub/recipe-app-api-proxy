# Recipe App API Proxy

NGINX proxy app for our recipe app API.

## Usage

### Environment Variables

* `LISTEN_PORT` - Port on which to listen (default: `8000`)
* `APP_HOST` - App host to which to forward requests (default: `app`)
* `APP_PORT` - Port on host to which to forward requests (default: `9000`)
